from django.db import models
from django.contrib.auth.models import User
from accounts.models import CareDoctor, CarePatient
from django.dispatch import receiver
from django.db.models.signals import m2m_changed
from django.core.exceptions import ValidationError
from izzychat.models import ChatRoom, ChatConsumer

APPO_STATUS = (
    ('WA', 'Wating'),
    ('AP', 'Approved'),
    ('RJ', 'Rejected'),
    ('CP', 'Completed')
)

APPO_PRIORITY = (
    ('HI', 'High'),
    ('ME', 'Meduim'),
    ('LO', 'Low')
)

# Create your models here.
class ContactProfile(models.Model):
    user = models.OneToOneField(User, related_name='contact_profile', on_delete=models.CASCADE)
    identifier = models.CharField(max_length=128, unique=True)
    fcm_token = models.CharField(max_length=512, blank=True)
    phone = models.CharField(max_length=10, blank=True)

    class Meta:
        db_table = 'contact_profile'

    def __str__(self):
        return self.identifier

class Contact(models.Model):
    contact_profiles = models.ManyToManyField(ContactProfile, related_name='contacts', blank=True)
    contact_name = models.CharField(max_length=128, blank=True)
    contact_img = models.ImageField(upload_to='contacts/', blank=True)

    class Meta:
        db_table = 'contacts'

    def __str__(self):
        st = ''
        for c_p in self.contact_profiles.all():
            st += c_p.identifier + ', '
        return st

class PatientAppointment(models.Model):
    patient = models.ForeignKey(CarePatient, related_name='appointments', on_delete=models.CASCADE)
    doctors = models.ManyToManyField(CareDoctor, related_name='appointments')
    appo_status = models.CharField(max_length=2, choices=APPO_STATUS, default='WA')
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    priority = models.CharField(max_length=2, choices=APPO_PRIORITY, default='LO')
    note = models.CharField(max_length=128, blank=True)

    class Meta:
        db_table = 'patient_appointment'

    def __str__(self):
        doctors = ''
        for doc in self.doctors.all():
            doctors += str(doc) + ', '
        return 'PAT: {} > DOC: {}'.format(str(self.patient), doctors)

# @receiver(m2m_changed, sender=PatientContactBook.doctor_contacts.through)
# def patient_doctor_contacts_changed(sender, **kwargs):
#     if kwargs['instance'].doctor_contacts.count() > 3:
#         raise ValidationError("Patient cannot have more than 3 doctor contacts")
#     for doc_contact in kwargs['instance'].doctor_contacts.all():
#         if kwargs['instance'].patient.contact_profile not in doc_contact.doctor.contact_book.patient_contacts.all():
#             doc_contact.doctor.contact_book.patient_contacts.add(kwargs['instance'].patient.contact_profile)
#             doc_contact.doctor.contact_book.save()
        # patient_c_book = kwargs['instance']
        # pat_consumer = ChatConsumer.objects.get(user=patient_c_book.patient.user)
        # doc_consumer = ChatConsumer.objects.get(user=doc_contact.doctor.user)
        # room = ChatRoom.objects.create()
        # room.consumers.add(pat_consumer)
        # room.consumers.add(doc_consumer)
        # room.save()

# @receiver(m2m_changed, sender=DoctorContactBook.doctor_contacts.through)
# def doctor_doctor_contacts_changed(sender, **kwargs):
#     for doc_contact in kwargs["instance"].doctor_contacts.all():
#         if kwargs['instance'].doctor.contact_profile not in doc_contact.doctor.contact_book.doctor_contacts.all():
#             doc_contact.doctor.contact_book.doctor_contacts.add(kwargs['instance'].doctor.contact_profile)
#             doc_contact.doctor.contact_book.save()
        # doc_c_book = kwargs['instance']
        # doc_owner_consumer = ChatConsumer.objects.get(user=doc_c_book.doctor.user)
        # doc_consumer = ChatConsumer.objects.get(user=doc_contact.doctor.user)
        # room = ChatRoom.objects.create()
        # room.consumers.add(doc_owner_consumer)
        # room.add(doc_consumer)
        # room.save()