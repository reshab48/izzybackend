from rest_framework import serializers, exceptions
from izzychat.models import ChatRoom, ChatConsumer, ChatMessage, ChatNote

class ChatRoomSerializer(serializers.ModelSerializer):
    room_name = serializers.SerializerMethodField()
    room_type = serializers.SerializerMethodField()
    room_image = serializers.SerializerMethodField()
    last_message = serializers.SerializerMethodField()
    count_unread = serializers.SerializerMethodField()
    user_email = serializers.SerializerMethodField()

    class Meta:
        model = ChatRoom
        fields = ('id', 'room_name', 'room_type', 'room_image','last_message', 'count_unread', 'user_email')

    def get_room_name(self, obj):
        user = self.context['request'].user
        if obj.room_name != None and obj.consumers.all().count() > 2:
            return obj.room_name
        consumer = ChatConsumer.objects.get(user=user)
        for consumr in obj.consumers.all():
            if consumr != consumer:
                user = consumr.user
                if hasattr(user, 'care_patient'):
                    return '{} {}'.format(user.first_name, user.last_name)
                else:
                    return 'Dr. {} {}'.format(user.first_name, user.last_name)

    def get_room_type(self, obj):
        user = self.context['request'].user
        if obj.consumers.all().count() > 2:
            return "Care Group"
        consumer = ChatConsumer.objects.get(user=user)
        for consumr in obj.consumers.all():
            if consumr != consumer:
                user = consumr.user
                if hasattr(user, 'care_patient'):
                    return 'Care Patient'
                else:
                    return 'Care Doctor'

    def get_room_image(self, obj):
        user = self.context['request'].user
        if obj.consumers.all().count() > 2:
            try:
                return obj.room_img.url
            except:
                return ""
        consumer = ChatConsumer.objects.get(user=user)
        for consumr in obj.consumers.all():
            if consumr != consumer:
                user = consumr.user
                if hasattr(user, 'care_patient'):
                    return user.care_patient.patient_profile.prof_pic.url
                else:
                    return user.care_doctor.prof_pic.url

    def get_last_message(self, obj):
        return ChatMessage.objects.filter(chat_room=obj).first().data if ChatMessage.objects.filter(chat_room=obj).first() else 'No messages'

    def get_count_unread(self, obj):
        count = 0
        consumer = ChatConsumer.objects.get(user=self.context['request'].user)
        for consumr in obj.consumers.all():
            if consumr != consumer:
                count += ChatMessage.objects.filter(chat_room=obj, consumer=consumr, viewed=False).count()
        return count

    def get_user_email(self, obj):
        doc_consumer = None
        for consumr in obj.consumers.all():
            if hasattr(consumr.user, 'care_patient'):
                return consumr.user.email
            doc_consumer = consumr
        return doc_consumer.user.email

class ChatMessageSerializer(serializers.ModelSerializer):
    type = serializers.SerializerMethodField()
    user = serializers.SerializerMethodField()
    user_name = serializers.SerializerMethodField()
    time = serializers.SerializerMethodField()
    date = serializers.SerializerMethodField()

    class Meta:
        model = ChatMessage
        fields = ('type', 'id', 'm_type', 'data', 'viewed', 'created', 'user', 'user_name', 'time', 'date')

    def get_type(self, obj):
        return 'chat_message'

    def get_user(self, obj):
        return obj.consumer.user.email

    def get_user_name(self, obj):
        if hasattr(obj.consumer.user, 'care_patient'):
            return obj.consumer.user.first_name
        else:
            return 'Dr. {}'.format(obj.consumer.user.first_name)
    
    def get_time(self, obj):
        hour = obj.created.hour if obj.created.hour <= 12 else obj.created.hour - 12
        am_pm = 'AM' if obj.created.hour < 12 else 'PM'
        return '{:02d}:{:02d} {}'.format(hour, obj.created.minute, am_pm)

    def get_date(self, obj):
        return '{:02d}/{:02d}/{}'.format(obj.created.day, obj.created.month, obj.created.year)

class ChatNoteSerializer(serializers.ModelSerializer):
    prof_pic = serializers.SerializerMethodField()
    user_name = serializers.SerializerMethodField()
    data = serializers.SerializerMethodField()

    class Meta:
        model = ChatNote
        fields = ('id', 'prof_pic', 'user_name', 'created', 'data')

    def get_prof_pic(self, obj):
        if hasattr(obj.chat_message.consumer.user, 'care_patient'):
            return obj.chat_message.consumer.user.care_patient.patient_profile.prof_pic.url
        return obj.chat_message.consumer.user.care_doctor.prof_pic.url

    def get_user_name(self, obj):
        if hasattr(obj.chat_message.consumer.user, 'care_patient'):
            return '{} {}'.format(obj.chat_message.consumer.user.first_name, obj.chat_message.consumer.user.last_name)
        return 'Dr. {} {}'.format(obj.chat_message.consumer.user.first_name, obj.chat_message.consumer.user.last_name)

    def get_data(self, obj):
        return obj.chat_message.data