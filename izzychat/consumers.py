from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
from izzychat.models import ChatMessage, ChatRoom, ChatConsumer
from pyfcm import FCMNotification
from django.conf import settings
import json

class IzzyChatConsumer(WebsocketConsumer):

    def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'chat_%s' % self.room_name
        self.room = ChatRoom.objects.get(id=self.room_name)
        self.user = self.scope['user']
        self.izzy_consumer = ChatConsumer.objects.get(user=self.user)
        self.izzy_consumer.online = True
        self.izzy_consumer.save()
        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name
        )
        self.accept()

    def disconnect(self, close_code):
        self.izzy_consumer.online = False
        self.izzy_consumer.save()
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name
        )

    def receive(self, text_data):
        data_json = json.loads(text_data)
        m_type = data_json['m_type']
        message_data = data_json['data']
        chat_msg = None
        print(data_json)
        if m_type == 'UPD':
            m_id = data_json['id']
            chat_msg = ChatMessage.objects.get(id=m_id)
            chat_msg.viewed = True
            chat_msg.save()
        else:
            chat_msg = ChatMessage.objects.create(chat_room=self.room, consumer=self.izzy_consumer, m_type=m_type, data=message_data)
            hour = chat_msg.created.hour if chat_msg.created.hour <= 12 else chat_msg.created.hour - 12
            am_pm = 'AM' if chat_msg.created.hour < 12 else 'PM'
            time = '{:02d}:{:02d} {}'.format(hour, chat_msg.created.minute, am_pm)
            date = '{:02d}/{:02d}/{}'.format(chat_msg.created.day, chat_msg.created.month, chat_msg.created.year)
            push_service = FCMNotification(api_key=settings.FCM_SERVER_KEY)
            for consumer in self.room.consumers.all():
                if self.izzy_consumer != consumer:
                    if not consumer.online:
                        data_message = {
                            'data' : message_data,
                            'user_name' : '{} {}'.format(chat_msg.consumer.user.first_name, chat_msg.consumer.user.last_name) if hasattr(chat_msg.consumer.user, 'care_patient') else 'Dr. {} {}'.format(chat_msg.consumer.user.first_name, chat_msg.consumer.user.last_name),
                            'room' : str(self.room.id),
                            'room_name' : self.room.room_name if self.room.consumers.all().count() > 2 else None
                        }
                        fcm_resp = push_service.notify_single_device(registration_id=consumer.user.contact_profile.fcm_token, data_message=data_message, content_available=True)
                        print(fcm_resp)
            async_to_sync(self.channel_layer.group_send)(
                self.room_group_name,
                {
                    'type' : 'chat_message',
                    'id' : chat_msg.id,
                    'm_type' : m_type,
                    'data' : message_data,
                    'viewed' : chat_msg.viewed,
                    'created' : str(chat_msg.created),
                    'user' : chat_msg.consumer.user.email,
                    'user_name' : chat_msg.consumer.user.first_name if hasattr(chat_msg.consumer.user, 'care_patient') else 'Dr. {}'.format(chat_msg.consumer.user.first_name),
                    'time' : time,
                    'date' : date,
                }
            )

    def chat_message(self, data):
        data_json = json.dumps(data)
        print(data_json)
        self.send(text_data=data_json)