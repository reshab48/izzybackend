from django.contrib import admin
from .models import ChatConsumer, ChatRoom, ChatMessage

# Register your models here.
admin.site.register(ChatConsumer)
admin.site.register(ChatRoom)
admin.site.register(ChatMessage)