from django.shortcuts import render, get_object_or_404
from izzypay.models import Payment, PaymentMode
from izzypay.serializers import CardAddSerializer
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from rest_framework import exceptions, status
from rest_framework.response import Response
from django.contrib.auth.models import User
from django.contrib.sites.shortcuts import get_current_site
from accounts.models import Membership
from django.conf import settings
import stripe

# Create your views here.
class PaymentCardSetView(APIView):
    permission_classes = [AllowAny,]
    name = 'payment-card-set'

    def post(self, request, format=None):
        serializer = CardAddSerializer(data=request.data)
        if serializer.is_valid():
            try:
                user = User.objects.get(email=serializer.data.get('email'))
            except Exception as e:
                print(str(e))
                raise exceptions.NotFound("User not found")
            if not hasattr(user, 'care_patient'):
                raise exceptions.PermissionDenied("User is not a care patient.")
            stripe.api_key = settings.STRIPE_SECRET_KEY
            try:
                token = stripe.Token.create(card={"number" : serializer.data.get('card_number'), "exp_month" : serializer.data.get('exp_month'), "exp_year" : serializer.data.get('exp_year'), "cvc" : serializer.data.get('cvc')})
                if not hasattr(user.care_patient, 'payment_mode'):
                    charge = stripe.Charge.create(amount=100, currency="usd", source=token.id, description="Card set charge for: {}".format(user.email))
                else:
                    charge = stripe.Charge.create(amount=50, currency="usd", source=token.id, description="Card set charge for: {}".format(user.email))
            except stripe.error.CardError as e:
                body = e.json_body
                err = body.get('error', {})
                raise exceptions.ParseError(err.get('message'))
            except stripe.error.RateLimitError as e:
                raise exceptions.ParseError("Payment failed, please try again in some time")
            except stripe.error.InvalidRequestError as e:
                raise exceptions.ParseError("Invalid request, check your values and try again")
            except stripe.error.AuthenticationError as e:
                #send an email to admins
                raise exceptions.ParseError("Payment failed, please try again in some time")
            except stripe.error.APIConnectionError as e:
                #send email to admins
                raise exceptions.ParseError("Payment failed, please try again in some time")
            except stripe.error.StripeError as e:
                #send email to admins
                raise exceptions.ParseError("Payment failed, please try again in some time")
            except Exception as e:
                print(str(e))
                raise exceptions.ParseError("Invalid card details, check your values and try again")
            if not hasattr(user.care_patient, 'payment_mode'):
                PaymentMode.objects.create(care_patient=user.care_patient, pay_monthly=serializer.data.get('pay_monthly'), card_number=serializer.data.get('card_number'), exp_month=serializer.data.get('exp_month'), exp_year=serializer.data.get('exp_year'), cvc=serializer.data.get('cvc'), brand=serializer.data.get('brand'))
            else:
                pay_mode = user.care_patient.payment_mode
                pay_mode.card_number = serializer.data.get('card_number')
                pay_mode.exp_month = serializer.data.get('exp_month')
                pay_mode.exp_year = serializer.data.get('exp_year')
                pay_mode.cvc = serializer.data.get('cvc')
                pay_mode.brand = serializer.data.get('brand')
                pay_mode.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)