from channels.auth import AuthMiddlewareStack
from rest_auth.models import TokenModel
from django.contrib.auth.models import AnonymousUser
from channels import exceptions


class TokenAuthMiddleware:

    def __init__(self, inner):
        self.inner = inner

    def __call__(self, scope):
        headers = dict(scope['headers'])
        print(headers)
        if b'authorization' in headers:
            try:
                token_name, token_key = headers[b'authorization'].decode().split(' ')
                if token_name == 'Token':
                    token = TokenModel.objects.get(key=token_key)
                    scope['user'] = token.user
            except Token.DoesNotExist:
                # scope['user'] = AnonymousUser()
                raise exceptions.DenyConnection()
        else:
            raise exceptions.DenyConnection()
        return self.inner(scope)

TokenAuthMiddlewareStack = lambda inner: TokenAuthMiddleware(AuthMiddlewareStack(inner))