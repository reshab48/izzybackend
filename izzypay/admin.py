from django.contrib import admin
from izzypay.models import Payment, PaymentMode

# Register your models here.
admin.site.register(Payment)
admin.site.register(PaymentMode)