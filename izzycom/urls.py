from django.urls import path
from izzycom import views

urlpatterns = [
    path('api/v1/patient-video-room-create/', views.PatientVideoRoomCreateView.as_view(), name='patient-video-room-create'),
    path('api/v1/set-fcm-token/', views.SetFCMToken.as_view(), name='set-fcm-token'),
    path('api/v1/contact-list/', views.ListContacts.as_view(), name='contact-list'),
    path('api/v1/patient-appointment-create/', views.PatientAppointmentCreateView.as_view(), name='patient-appointment-create'),
    path('api/v1/patient-appointment-list/', views.PatientAppointmentListView.as_view(), name='patient-appointment-list'),
    path('api/v1/<int:id>/patient-appointment-action/', views.PatientAppointmentActionView.as_view(), name='patient-appointment-action'),
]