# Generated by Django 2.1.4 on 2019-01-08 11:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='caredoctor',
            name='prof_pic',
            field=models.ImageField(default='/Users/iodev/Desktop/izzyenv/izzybackend/izzyadmin/static/default/avatar.png', upload_to='doctors/pics/'),
        ),
        migrations.AlterField(
            model_name='izzypatientprofile',
            name='prof_pic',
            field=models.ImageField(default='/Users/iodev/Desktop/izzyenv/izzybackend/izzyadmin/static/default/avatar.png', upload_to='patients/pics/'),
        ),
    ]
