from rest_framework import permissions

class IsCareDoctor(permissions.BasePermission):

    def has_permission(self, request, view):
        return True if hasattr(request.user, 'care_doctor') else False

class IsCarePatient(permissions.BasePermission):

    def has_permission(self, request, view):
        return True if hasattr(request.user, 'care_patient') else False

class IsCareDoctorOrPatient(permissions.BasePermission):
    
    def has_permission(self, request, view):
        return True if hasattr(request.user, 'care_doctor') or hasattr(request.user, 'care_patient') else False