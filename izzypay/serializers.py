from rest_framework import serializers

class CardAddSerializer(serializers.Serializer):
    email = serializers.EmailField()
    card_number = serializers.CharField(max_length=16)
    exp_month = serializers.IntegerField()
    exp_year = serializers.IntegerField()
    cvc = serializers.CharField(max_length=3)
    brand = serializers.CharField(max_length=50)
    pay_monthly = serializers.BooleanField(default=True)