from rest_framework import serializers
from izzycom.models import ContactProfile, Contact, PatientAppointment

class ContactProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContactProfile
        fields = ('identifier',)
        read_only_fields = ('identifier',)

class ContactSerializer(serializers.ModelSerializer):
    contact_name = serializers.SerializerMethodField()
    contact_image = serializers.SerializerMethodField()
    contact_type = serializers.SerializerMethodField()
    user_email = serializers.SerializerMethodField()

    class Meta:
        model = Contact
        fields = ('id', 'contact_name', 'contact_image', 'contact_type', 'user_email')
        read_only_fields = ('contact_name', 'contact_image', 'contact_profiles')

    def get_contact_name(self, obj):
        user = self.context['request'].user
        if obj.contact_profiles.all().count() > 2:
            return obj.contact_name
        for contact_prof in obj.contact_profiles.all():
            if contact_prof.user != user:
                user = contact_prof.user
                if hasattr(user, 'care_patient'):
                    return "{} {}".format(user.first_name, user.last_name)
                else:
                    return "Dr. {} {}".format(user.first_name, user.last_name)
    
    def get_contact_image(self, obj):
        user = self.context['request'].user
        if obj.contact_profiles.all().count() > 2:
            return obj.contact_img.url
        for contact_prof in obj.contact_profiles.all():
            if contact_prof.user != user:
                user = contact_prof.user
                if hasattr(user, 'care_patient'):
                    return user.care_patient.patient_profile.prof_pic.url
                else:
                    return user.care_doctor.prof_pic.url

    def get_contact_type(self, obj):
        for contact_prof in obj.contact_profiles.all():
            if hasattr(contact_prof.user, 'care_patient'):
                return 'care_patient'
        return 'care_doctor'

    def get_user_email(self, obj):
        doc_prof = None
        for contact_prof in obj.contact_profiles.all():
            if hasattr(contact_prof.user, 'care_patient'):
                return contact_prof.user.email
            doc_prof = contact_prof
        return doc_prof.user.email

class PatientAppointmentSerializer(serializers.ModelSerializer):
    prof_pic = serializers.SerializerMethodField()
    patient = serializers.SerializerMethodField()
    doctors = serializers.SerializerMethodField()
    start_time = serializers.SerializerMethodField()
    end_time = serializers.SerializerMethodField()

    class Meta:
        model = PatientAppointment
        fields = ('id', 'prof_pic', 'patient', 'doctors', 'appo_status', 'start_time', 'end_time', 'priority', 'note')
        read_only_fields = ('id', 'patient', 'doctors', 'appo_status', 'start_time', 'end_time', 'priority', 'note')

    def get_prof_pic(self, obj):
        return obj.patient.patient_profile.prof_pic.url

    def get_patient(self, obj):
        return '{} {}'.format(obj.patient.user.first_name, obj.patient.user.last_name)

    def get_doctors(self, obj):
        user = self.context['request'].user
        doctors = 'You'
        for doc in obj.doctors.all():
            if doc.user != user:
                doctors += ', Dr. {} {}'.format(doc.user.first_name, doc.user.last_name)
        return doctors

    def get_start_time(self, obj):
        hour = obj.start_time.hour if obj.start_time.hour <= 12 else obj.start_time.hour - 12
        am_pm = 'AM' if obj.start_time.hour < 12 else 'PM'
        time = '{:02d}:{:02d} {}'.format(hour, obj.start_time.minute, am_pm)
        return time

    def get_end_time(self, obj):
        hour = obj.end_time.hour if obj.end_time.hour <= 12 else obj.end_time.hour - 12
        am_pm = 'AM' if obj.end_time.hour < 12 else 'PM'
        time = '{:02d}:{:02d} {}'.format(hour, obj.end_time.minute, am_pm)
        return time