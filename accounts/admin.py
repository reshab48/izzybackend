from django.contrib import admin
from accounts.models import IzzyState, IzzyCountry, CarePatient, CareDoctor, Membership, IzzyPatientProfile, CareTeam, PatientPrescription

# Register your models here.
admin.site.register(Membership)
admin.site.register(IzzyState)
admin.site.register(IzzyCountry)
admin.site.register(CarePatient)
admin.site.register(IzzyPatientProfile)
admin.site.register(CareDoctor)
admin.site.register(CareTeam)
admin.site.register(PatientPrescription)