from django.shortcuts import render, get_object_or_404
from rest_framework import exceptions, generics, status
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from django.core.exceptions import MultipleObjectsReturned
from common import permissions
from rest_framework.views import APIView
from rest_framework import exceptions
from accounts.serializers import CarePatientSerializer, CarePatientProfileSerializer, IzzyStateSerializer, IzzyCountrySerializer, PrescriptionSerializer, PrescriptionListSerializer
from accounts.models import IzzyCountry, IzzyState, Membership, PatientPrescription
from django.contrib.auth.models import User
from izzypay.models import Payment
from rest_auth.views import LoginView, PasswordResetView, PasswordResetConfirmView
from django.contrib.sites.shortcuts import get_current_site
from common.rest_exceptions import PaymentRequiredException
from django.template.defaultfilters import slugify
from django.template.loader import render_to_string
from weasyprint import HTML
from django.conf import settings
from django.utils import timezone
from pathlib import Path
import json
import os

# Create your views here.
class CustomLoginView(LoginView):
    def get_response(self):
        if hasattr(self.user, 'care_patient'):
            if not hasattr(self.user.care_patient, 'payment_mode'):
                raise exceptions.PermissionDenied("Payment method not added, please add your payment method.")
            if self.user.care_patient.payments.filter(due=True).count() > 0:
                raise PaymentRequiredException()
        return super().get_response()

class IzzyPasswordResetView(PasswordResetView):
    pass

class IzzyPasswordResetConfirmView(PasswordResetConfirmView):
    pass

class CarePatientCreateView(generics.CreateAPIView):
    permission_classes = [AllowAny,]
    serializer_class = CarePatientSerializer
    name = 'create-care-patient'

class CarePatientProfileCreateView(generics.CreateAPIView):
    permission_classes = [AllowAny,]
    parser_classes = (MultiPartParser, FormParser)
    serializer_class = CarePatientProfileSerializer
    name = 'create-patient-profile'

class ListIzzyCountryView(generics.ListAPIView):
    permission_classes = [AllowAny,]
    serializer_class = IzzyCountrySerializer
    name = 'list-izzy-country'

    def get_queryset(self):
        return IzzyCountry.objects.all()

class ListIzzyStateView(generics.ListAPIView):
    permission_classes = [AllowAny,]
    serializer_class = IzzyStateSerializer
    name = 'list-izzy-state'

    def get_queryset(self):
        return IzzyState.objects.all()

class MembershipDetailView(APIView):
    permission_classes = [AllowAny,]
    name = 'membership-detail'

    def get(self, request, age):
        membership = get_object_or_404(Membership, site=get_current_site(request))
        if membership.charge_actual:
            if age < 20:
                monthly = membership.actual_teen_price
                yearly = membership.actual_teen_yearly_price
            else:
                monthly = membership.actual_adult_price
                yearly = membership.actual_adult_yearly_price
        else:
            if age < 20:
                monthly = membership.intro_teen_price
                yearly = membership.intro_teen_yearly_price
            else:
                monthly = membership.intro_adult_price
                yearly = membership.intro_adult_yearly_price
        return Response({'monthly' : monthly, 'yearly' : yearly})

class PrescriptionCreateView(APIView):
    permission_classes = [IsAuthenticated, permissions.IsCareDoctor]
    name = 'prescription-create-view'

    def post(self, request, format=None):
        serializer = PrescriptionSerializer(data=request.data)
        pdf_path = '{}prescriptions'.format(settings.MEDIA_ROOT)
        if not os.path.isdir(pdf_path):
            os.mkdir(pdf_path)
        if serializer.is_valid():
            doctor = request.user.care_doctor
            patient = get_object_or_404(User, email=serializer.data.get('pat_email')).care_patient
            doc_name = 'Dr. {} {}'.format(request.user.first_name, request.user.last_name)
            pat_name = '{} {}'.format(patient.user.first_name, patient.user.last_name)
            html = render_to_string('prescription/template.html', {'doc_name': doc_name, 'pat_name': pat_name, 'pat_age': patient.patient_profile.age, 'meds' : serializer.data.get('meds'), 'signature': doctor.signature.url, 'date' : str(timezone.now().date())})
            file_name = '{}-{}-{}.pdf'.format(slugify(doc_name), slugify(pat_name), str(timezone.now().date()))
            if not Path('{}/{}'.format(pdf_path, file_name)).exists():
                pdf_media_path = 'prescriptions/{}'.format(file_name)
                PatientPrescription.objects.create(doctor=doctor, patient=patient, pdf_path=pdf_media_path)
            HTML(string=html, base_url=request.build_absolute_uri()).write_pdf('{}/{}'.format(pdf_path, file_name))
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class DoctorPatientsListView(APIView):
    permission_classes = [IsAuthenticated, permissions.IsCareDoctor]
    name = 'doctor-patients-list-view'

    def get(self, request):
        patients = []
        doctor = request.user.care_doctor
        for care_team in doctor.care_teams.all():
            for c_patient in care_team.care_patients.all():
                pat = {}
                pat['name'] = '{} {}'.format(c_patient.user.first_name, c_patient.user.last_name)
                pat['prof_pic'] = request.build_absolute_uri(c_patient.patient_profile.prof_pic.url)
                pat['email'] = c_patient.user.email
                patients.append(pat)
        return Response(data=patients, status=status.HTTP_200_OK)

class PrescriptionListView(generics.ListAPIView):
    permission_classes = [IsAuthenticated, permissions.IsCareDoctorOrPatient]
    serializer_class = PrescriptionListSerializer
    name = 'prescription-list-view'

    def get_queryset(self):
        if hasattr(self.request.user, 'care_patient'):
            return PatientPrescription.objects.filter(patient=self.request.user.care_patient).distinct()
        return PatientPrescription.objects.filter(doctor=self.request.user.care_doctor).distinct()

class ProfileDetailView(APIView):
    permission_classes = [IsAuthenticated, permissions.IsCareDoctorOrPatient]
    name = 'profile-detail-view'

    def post(self, request, format=None):
        try:
            user_email = request.data['email']
            user = User.objects.get(email=user_email)
        except KeyError:
            raise exceptions.APIException(detail="Email cannot be blank", code=400)
        except Exception as e:
            raise exceptions.APIException(detail=str(e), code=500)
        if hasattr(user, 'care_patient'):
            profile = {}
            pat_profile = user.care_patient.patient_profile
            profile['sex'] = pat_profile.sex
            profile['age'] = pat_profile.age
            profile['height'] = pat_profile.height
            profile['weight'] = pat_profile.weight
            profile['allergies'] = pat_profile.allergies
            profile['medication_allergies'] = pat_profile.medication_allergies
            profile['heredity_diseases'] = pat_profile.heredity_diseases
            return Response(data=profile, status=status.HTTP_200_OK)
        profile = {}
        doc = user.care_doctor
        profile['gender'] = doc.gender
        profile['age'] = doc.age
        profile['bio'] = doc.bio
        return Response(data=profile, status=status.HTTP_200_OK)