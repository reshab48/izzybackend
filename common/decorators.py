from functools import wraps
from django.shortcuts import redirect

def staff_only(function):
    @wraps(function)
    def wrap(request, *args, **kwargs):
        if not request.user.is_staff:
            return redirect('izzyadmin:login')
        else:
            return function(request, *args, **kwargs)
    return wrap