from django.db import models
from django.contrib.auth.models import User
import uuid

MESSAGE_TYPES = (
    ('MSG', 'Message'),
    ('FIL', 'File')
)

# Create your models here.
class ChatConsumer(models.Model):
    user = models.OneToOneField(User, related_name='chat_consumer', on_delete=models.CASCADE)
    online = models.BooleanField(default=False)

    class Meta:
        db_table = 'izzy_chat_consumer'

    def __str__(self):
        return str(self.user)

class ChatRoom(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    consumers = models.ManyToManyField(ChatConsumer, related_name='chat_rooms', blank=True)
    room_name = models.CharField(max_length=128, blank=True)
    room_img = models.ImageField(upload_to='chat/rooms/images/', blank=True)

    class Meta:
        db_table = 'izzy_chat_room'

    def __str__(self):
        st = ''
        for con in self.consumers.all():
            st += con.user.username + ', '
        return st

class ChatMessage(models.Model):
    chat_room = models.ForeignKey(ChatRoom, related_name='chat_messages', on_delete=models.CASCADE)
    consumer = models.ForeignKey(ChatConsumer, related_name='chat_messages', on_delete=models.CASCADE)
    m_type = models.CharField(max_length=3, choices=MESSAGE_TYPES)
    data = models.CharField(max_length=1024)
    viewed = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'izzy_chat_message'
        ordering = ('-created',)

    def __str__(self):
        return str(self.id)

class ChatNote(models.Model):
    user = models.ForeignKey(User, related_name='chat_notes', on_delete=models.CASCADE)
    chat_message = models.ForeignKey(ChatMessage, related_name='chat_notes', on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'izzy_chat_notes'
        ordering = ('-created',)

    def __str__(self):
        return '<ChatNote: {}>'.format(str(self.user))