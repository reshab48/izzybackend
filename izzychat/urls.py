from django.urls import path
from izzychat import views

urlpatterns = [
    path('api/v1/list-user-chat-rooms/', views.ListUserChatRooms.as_view(), name='list-user-chat-rooms'),
    path('api/v1/<str:uid>/list-room-chat-messages/', views.ListRoomChatMessages.as_view(), name='list-room-chat-messages'),
    path('api/v1/save-chat-message/', views.SaveChatMessage.as_view(), name='save-chat-message'),
    path('api/v1/list-chat-notes/', views.ListChatNotes.as_view(), name='list-chat-notes'),
]