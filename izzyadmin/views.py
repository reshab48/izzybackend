from django.shortcuts import render, redirect
from django.contrib.auth.views import LoginView
from izzyadmin.forms import CustomAuthenticationForm
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from accounts.models import CarePatient, CareTeam, CareDoctor
from izzycom.models import PatientAppointment
from django.contrib.auth.decorators import login_required
from izzyadmin.forms import UserForm, CareDoctorForm
from django.contrib.auth.models import User
from common.decorators import staff_only

# Create your views here.
class CustomLoginView(LoginView):
    form_class = CustomAuthenticationForm

@login_required
@staff_only
def dashboard(request):
    total_registrations = CarePatient.objects.all().count()
    total_appointments = PatientAppointment.objects.all().count()
    total_care_teams = CareTeam.objects.all().count()
    return render(request, 'admin/dashboard.html', {'total_registrations' : total_registrations, 'total_appointments' : total_appointments, 'total_care_teams' : total_care_teams})

@login_required
@staff_only
def izzy_doctors_list(request):
    doctors_list = CareDoctor.objects.all()
    page = request.GET.get('page', 1)
    paginator = Paginator(doctors_list, 10)
    try:
        doctors = paginator.page(page)
    except PageNotAnInteger:
        doctors = paginator.page(1)
    except EmptyPage:
        doctors = paginator.page(paginator.num_pages)
    return render(request, 'admin/view_doctor_list.html', {'doctors' : doctors})

@login_required
@staff_only
def create_izzy_doctor(request):
    if request.method == 'POST':
        user_form = UserForm(request.POST)
        care_doctor_form = CareDoctorForm(data=request.POST, files=request.FILES)
        if all([user_form.is_valid(), care_doctor_form.is_valid()]):
            cd = user_form.cleaned_data
            user = User.objects.create(username=cd.get('email'), email=cd.get('email'), first_name=cd.get('first_name'), last_name=cd.get('last_name'))
            doc_cd = care_doctor_form.cleaned_data
            doc_cd['user'] = user
            CareDoctor.objects.create(**doc_cd)
            return redirect('izzyadmin:doctors-list')
    else:
        user_form = UserForm()
        care_doctor_form = CareDoctorForm()
    return render(request, 'admin/upload_doctor.html', {'user_form' : user_form, 'care_doctor_form' : care_doctor_form})