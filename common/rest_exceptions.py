from rest_framework.exceptions import APIException
from rest_framework import status

class PaymentRequiredException(APIException):
    status_code = status.HTTP_402_PAYMENT_REQUIRED
    default_detail = "Payment not done, please complete your payment."
    default_code = 'payment_required'