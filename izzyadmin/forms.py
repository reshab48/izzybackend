from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from accounts.models import CareDoctor
from django import forms

class CustomAuthenticationForm(AuthenticationForm):
    def confirm_login_allowed(self, user):
        super().confirm_login_allowed(user)
        if not user.is_staff:
            raise forms.ValidationError("Please enter the correct username and password for a staff account. Note that both fields may be case-sensitive.", code='not_staff')

class UserForm(UserCreationForm):
    first_name = forms.CharField(max_length=128, required=True)
    last_name = forms.CharField(max_length=128, required=True)
    email = forms.EmailField(required=True)

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'password1', 'password2')

class CareDoctorForm(forms.ModelForm):
    class Meta:
        model = CareDoctor
        exclude = ('id', 'user', 'created', 'updated')

    def __init__(self, *args, **kwargs):
        super(CareDoctorForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'