from django.apps import AppConfig


class IzzypayConfig(AppConfig):
    name = 'izzypay'
