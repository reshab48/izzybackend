from django.shortcuts import render, get_object_or_404
from django.db.models import Count, Q
from rest_framework.views import APIView
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework import status, exceptions
from django.utils.dateparse import parse_datetime
from django.db.models import Prefetch
from common import permissions
from twilio.jwt.access_token import AccessToken
from twilio.jwt.access_token.grants import VideoGrant
from pyfcm import FCMNotification
from izzycom.serializers import ContactSerializer, PatientAppointmentSerializer
from izzycom.models import ContactProfile, Contact, PatientAppointment
from accounts.models import CarePatient, CareDoctor
from twilio.rest import Client
from django.conf import settings
from django.utils import timezone
from datetime import timedelta
import random
import string
import json

# Create your views here.
class SetFCMToken(APIView):
    permission_classes = (IsAuthenticated, permissions.IsCareDoctorOrPatient)
    name = 'set-fcm-token'

    def post(self, request, format=None):
        reg_id = request.data['reg_id']
        if type(reg_id) != str:
            raise exceptions.ParseError("JSON syntax error")
        request.user.contact_profile.fcm_token = reg_id
        request.user.contact_profile.save()
        return Response(status=status.HTTP_204_NO_CONTENT)

class PatientVideoRoomCreateView(APIView):
    permission_classes = (IsAuthenticated, permissions.IsCarePatient)
    name = 'patient-video-room-create'

    def post(self, request, format=None):
        try:
            call_type = request.data['type']
            contact_id = request.data['id']
        except Exception as e:
            print(e)
            raise exceptions.APIException(detail=str(e), code=400)
        if type(call_type) != str:
            raise exceptions.APIException(detail="JSON syntax error, type must be string", code=400)
        if type(contact_id) != int:
            raise exceptions.APIException(detail="JSON syntax error, id must be integer", code=400)
        now = timezone.now()
        doc_c_ps = []
        docs = []
        try:
            contact = get_object_or_404(Contact, id=contact_id)
        except Exception as e:
            print(str(e))
            raise exceptions.APIException(detail="Contact not found", code=404)
        for prof in contact.contact_profiles.all():
            if prof.user != request.user:
                doc_c_ps.append(prof)
                docs.append(prof.user.care_doctor)
        today = timezone.datetime(year=now.year, month=now.month, day=now.day, hour=0, minute=0, second=0)
        tomorrow = today + timedelta(days=1)
        appos = request.user.care_patient.appointments.all().annotate(count=Count('doctors')).filter(count=len(docs),  start_time__gte=today, end_time__lte=tomorrow)
        for doc in docs:
            appos = appos.filter(doctors__pk=doc.pk)
        if len(appos) < 1:
            raise exceptions.NotFound("Appointment not found")
        else:
            try:
                appo = appos.get(start_time__lte=now, end_time__gte=now)
                if appo.appo_status != 'AP':
                    raise exceptions.PermissionDenied('Your appointment is waiting to be accepted' if appo.appo_status == 'WA' else 'Your appointment has been marked completed')
            except PatientAppointment.DoesNotExist:
                appo = appos[0]
                hour = appo.start_time.hour if appo.start_time.hour <= 12 else appo.start_time.hour - 12
                am_pm = 'AM' if appo.start_time.hour < 12 else 'PM'
                time = '{:02d}:{:02d} {}'.format(hour, appo.start_time.minute, am_pm)
                raise exceptions.PermissionDenied('Appointment set at: {}'.format(time))
        push_service = FCMNotification(api_key=settings.FCM_SERVER_KEY)
        twilio_client = Client(settings.TWILIO_ACCOUNT_SID, settings.TWILIO_AUTH_TOKEN)
        while True:
            try:
                ran_str = ''.join(random.sample(string.ascii_lowercase*6, 6))
                room_u_name = '{}-room-{}'.format(request.user.username, ran_str)
                meeting_room = twilio_client.video.rooms.create(type='group', unique_name=room_u_name, max_participants=len(doc_c_ps)+1)
                break
            except Exception as e:
                print(str(e))
                pass
        data_message = None
        for doc_c_p in doc_c_ps:
            twilio_token = AccessToken(settings.TWILIO_ACCOUNT_SID, settings.TWILIO_API_SID, settings.TWILIO_API_SECRET, identity=doc_c_p.identifier)
            twilio_token.add_grant(VideoGrant(room=meeting_room.unique_name))
            print(twilio_token.to_jwt())
            data_message = {}
            if call_type == "video":
                data_message['video_token'] = twilio_token.to_jwt().decode('utf-8')
            else:
                data_message['voice_token'] = twilio_token.to_jwt().decode('utf-8')
            data_message['name'] = '{} {}'.format(request.user.first_name, request.user.last_name)
            data_message['img'] = request.user.care_patient.patient_profile.prof_pic.url
            data_message['email'] = request.user.email
            fcm_resp = push_service.notify_single_device(registration_id=doc_c_p.fcm_token, data_message=data_message, content_available=True)
            print(fcm_resp)
        twilio_pat_token = AccessToken(settings.TWILIO_ACCOUNT_SID, settings.TWILIO_API_SID, settings.TWILIO_API_SECRET, identity=request.user.contact_profile.identifier)
        twilio_pat_token.add_grant(VideoGrant(room=meeting_room.unique_name))
        if call_type == "video":
            data_message = {'video_token' : twilio_pat_token.to_jwt().decode('utf-8')}
        else:
            data_message = {'voice_token' : twilio_pat_token.to_jwt().decode('utf-8')}
        return Response(data_message)

class ListContacts(generics.ListAPIView):
    permission_classes = (IsAuthenticated, permissions.IsCareDoctorOrPatient)
    serializer_class = ContactSerializer
    name = 'contact-list'

    def get_queryset(self):
        return Contact.objects.prefetch_related(Prefetch('contact_profiles', queryset=ContactProfile.objects.select_related('user', 'user__care_doctor', 'user__care_patient', 'user__care_patient__patient_profile'))).filter(contact_profiles__in=[self.request.user.contact_profile]).distinct()

class PatientAppointmentCreateView(APIView):
    permission_classes = (IsAuthenticated, permissions.IsCarePatient)
    name = 'patient-appointment-create'

    def post(self, request, format=None):
        print(request.data)
        doc_idens = request.data['idens']
        start_time = request.data['start_time']
        priority = request.data['priority']
        note = request.data['note']
        if type(doc_idens) != list or type(start_time) != str or type(priority) != str or type(note) != str:
            raise exceptions.ParseError("JSON syntax error")
        try:
            start_time = parse_datetime('{} {}'.format(str(timezone.now().date()), start_time))
        except Exception as e:
            print(str(e))
            raise exceptions.ParseError("Invalid time format use: hh:mm:ss")
        if len(priority) > 2 or priority not in ['HI', 'ME', 'LO']:
            raise exceptions.ParseError("Invalid priority value, use: HI for High, ME for Medium, LO for Low")
        end_time = start_time + timedelta(hours=1)
        push_service = FCMNotification(api_key=settings.FCM_SERVER_KEY)
        appo = PatientAppointment.objects.create(patient=request.user.care_patient, start_time=start_time, end_time=end_time, priority=priority, note=note)
        for iden in doc_idens:
            try:
                cp = ContactProfile.objects.get(identifier=iden)
                appo.doctors.add(cp.user.care_doctor)
                data_message = {'appointment' : 'Appointment created for {} {}'.format(request.user.first_name, request.user.last_name)}
                fcm_resp = push_service.notify_single_device(registration_id=cp.fcm_token, data_message=data_message, content_available=True)
                print(fcm_resp)
            except Exception as e:
                print(str(e))
                raise exceptions.ParseError("Could not create appointment, try again")
        appo.save()
        return Response(status=status.HTTP_204_NO_CONTENT)

class PatientAppointmentListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated, permissions.IsCareDoctor)
    serializer_class = PatientAppointmentSerializer
    name = 'patient-appointment-list-view'

    def get_queryset(self):
        now = timezone.now()
        today = timezone.datetime(year=now.year, month=now.month, day=now.day, hour=0, minute=0, second=0)
        tomorrow = today + timedelta(days=1)
        return PatientAppointment.objects.filter(doctors__in=[self.request.user.care_doctor], start_time__gte=today, end_time__lte=tomorrow).filter(Q(appo_status='WA') | Q(appo_status='AP'))

class PatientAppointmentActionView(APIView):
    permission_classes = (IsAuthenticated, permissions.IsCareDoctor)
    name = 'patient-appointment-action-view'

    def post(self, request, id, format=None):
        action = request.data['action']
        if type(action) != str:
            raise exceptions.ParseError("JSON syntax error")
        appo = get_object_or_404(PatientAppointment, id=id)
        if action == 'AP':
            appo.appo_status = action
        elif action == 'CP':
            appo.appo_status = action
        elif action == 'RJ':
            appo.appo_status = action
        else:
            raise exceptions.ParseError("Invalid Action")
        push_service = FCMNotification(api_key=settings.FCM_SERVER_KEY)
        if action == 'CP':
            doc_action_message = 'is marked completed'
            pat_action_message = 'marked as completed'
        if action == 'AP':
            doc_action_message = 'approved'
            pat_action_message = 'approved'
        else:
            doc_action_message = 'is marked rejected'
            pat_action_message = 'rejected'
        doc_message = {'appointment' : '{} {} @ Appointment {} by Dr. {} {}'.format(appo.patient.user.first_name, appo.patient.user.last_name, doc_action_message, request.user.first_name, request.user.last_name)}
        pat_message = {'appointment' : "Your appointment is {} by Dr. {} {}".format(pat_action_message, request.user.first_name, request.user.last_name)}
        for doc in appo.doctors.all():
            if doc != request.user.care_doctor:
                fcm_resp = push_service.notify_single_device(registration_id=doc.user.contact_profile.fcm_token, data_message=doc_message, content_available=True)
                print(fcm_resp)
        fcm_resp = push_service.notify_single_device(registration_id=appo.patient.user.contact_profile.fcm_token, data_message=pat_message, content_available=True)
        print(fcm_resp)
        appo.save()
        return Response(status=status.HTTP_204_NO_CONTENT)