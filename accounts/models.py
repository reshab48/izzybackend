from django.db import models
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django_countries.fields import CountryField
from django.core.exceptions import ValidationError
from django.utils.functional import lazy
from django.dispatch import receiver
from django.db.models.signals import post_save, m2m_changed, pre_delete
from django.core.exceptions import ValidationError
from django.db.models import Count
from izzychat.models import ChatRoom
from django.contrib.sites.models import Site
import uuid

USR_TYPES = (
    ('PAT', 'Patient'),
    ('DOC', 'Doctor')
)

GENS = (
    ('ML', 'Male'),
    ('FM', 'Female'),
)

# Create your models here.
class IzzyState(models.Model):
    name = models.CharField(max_length=128)
    slug = models.SlugField(unique=True)

    class Meta:
        db_table = 'izzy_state'

    def __str__(self):
        return self.name

class IzzyCountry(models.Model):
    country = CountryField()
    name = models.CharField(max_length=128, blank=True)

    class Meta:
        db_table = 'izzy_country'

    def __str__(self):
        return self.country.name

    def clean(self, *args, **kwargs):
        queryset = self._meta.model.objects.all()
        for q in queryset:
            if q.country.code == self.country.code and q.id != self.id:
                raise ValidationError('Country already exists')
        super(IzzyCountry, self).clean(*args, **kwargs)
    
    def save(self, *args, **kwargs):
        self.name = self.country.name
        super(IzzyCountry, self).save(*args, **kwargs)

class Membership(models.Model):
    site = models.OneToOneField(Site, related_name='membership', on_delete=models.CASCADE)
    izzy_country = models.OneToOneField(IzzyCountry, related_name='membership', on_delete=models.CASCADE)
    charge_actual = models.BooleanField(default=False)
    intro_adult_price = models.DecimalField(max_digits=100, decimal_places=2)
    intro_adult_yearly_price = models.DecimalField(max_digits=100, decimal_places=2)
    actual_adult_price = models.DecimalField(max_digits=100, decimal_places=2)
    actual_adult_yearly_price = models.DecimalField(max_digits=100, decimal_places=2)
    intro_teen_price = models.DecimalField(max_digits=100, decimal_places=2)
    intro_teen_yearly_price = models.DecimalField(max_digits=100, decimal_places=2)
    actual_teen_price = models.DecimalField(max_digits=100, decimal_places=2)
    actual_teen_yearly_price = models.DecimalField(max_digits=100, decimal_places=2)

    class Meta:
        db_table = 'membership'

    def __str__(self):
        return self.izzy_country.country.name

class CareDoctor(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.OneToOneField(User, related_name='care_doctor', on_delete=models.CASCADE)
    prof_pic = models.ImageField(upload_to='doctors/pics/', default='default/avatar.png')
    gender = models.CharField(max_length=2, choices=GENS)
    age = models.IntegerField()
    address = models.CharField(max_length=512)
    state = models.ForeignKey(IzzyState, related_name='doctors', blank=True, null=True, on_delete=models.DO_NOTHING)
    country = models.ForeignKey(IzzyCountry, related_name='doctors', on_delete=models.CASCADE)
    phone = models.CharField(max_length=10, blank=True)
    bio = models.TextField(blank=True)
    signature = models.ImageField(upload_to='doctors/signature/')
    created = models.DateTimeField(auto_now=True)
    updated = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('-created', '-updated')
        db_table = 'care_doctor'

    def __str__(self):
        return str(self.user)

class CareTeam(models.Model):
    identifier = models.CharField(max_length=128, unique=True)
    state = models.ForeignKey(IzzyState, related_name='care_teams', blank=True, null=True, on_delete=models.DO_NOTHING)
    country = models.ForeignKey(IzzyCountry, related_name='care_teams', on_delete=models.CASCADE)
    care_doctors = models.ManyToManyField(CareDoctor, related_name='care_teams')

    class Meta:
        db_table = 'care_team'

    def __str__(self):
        return self.identifier

class IzzyPatientProfile(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    sex = models.CharField(max_length=2, choices=GENS)
    age = models.IntegerField()
    prof_pic = models.ImageField(upload_to='patients/pics/', default='default/avatar.png')
    height = models.CharField(max_length=20)
    weight = models.CharField(max_length=20)
    allergies = models.CharField(max_length=512, default='None')
    medication_allergies = models.CharField(max_length=512, default='None')
    heredity_diseases = models.CharField(max_length=512, default='None')
    created = models.DateTimeField(auto_now=True)
    updated = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('-created', '-updated')
        db_table = 'izzy_patient_profile'

    def __str__(self):
        return str(self.id)

class CarePatient(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.OneToOneField(User, related_name='care_patient', on_delete=models.CASCADE)
    membership = models.ForeignKey(Membership, related_name='care_patients', on_delete=models.DO_NOTHING)
    care_team = models.ForeignKey(CareTeam, related_name='care_patients', on_delete=models.CASCADE)
    address = models.CharField(max_length=512)
    state = models.ForeignKey(IzzyState, related_name='patients', blank=True, null=True, on_delete=models.DO_NOTHING)
    country = models.ForeignKey(IzzyCountry, related_name='patients', on_delete=models.CASCADE)
    phone = models.CharField(max_length=10, blank=True)
    patient_profile = models.OneToOneField(IzzyPatientProfile, related_name='patient', on_delete=models.CASCADE, blank=True, null=True)
    created = models.DateTimeField(auto_now=True)
    updated = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('-created', '-updated')
        db_table = 'care_patient'

    def __str__(self):
        return str(self.user)

class PatientPrescription(models.Model):
    doctor = models.ForeignKey(CareDoctor, related_name='prescriptions', on_delete=models.CASCADE)
    patient = models.ForeignKey(CarePatient, related_name='prescriptions', on_delete=models.CASCADE)
    pdf_path = models.CharField(max_length=512)
    created = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'prescription'
        ordering = ('-created',)

    def __str__(self):
        return str(self.doctor)

@receiver(pre_delete, sender=User)
def user_pre_delete_function(sender, **kwargs):
    instance = kwargs['instance']
    instance.contact_profile.contacts.all().delete()
    instance.chat_consumer.chat_rooms.all().delete()
    if hasattr(instance, 'care_patient'):
        instance.care_patient.patient_profile.delete()

@receiver(post_save, sender=CarePatient)
def patient_modified_function(sender, instance, created, **kwargs):
    if created:
        from izzycom.models import ContactProfile, Contact
        from izzychat.models import ChatConsumer
        ContactProfile.objects.create(user=instance.user, identifier='izzycom-pat-{}'.format(instance.user.username))
        p_consumer = ChatConsumer.objects.create(user=instance.user)
        group_room = ChatRoom.objects.create()
        group_room.room_name = '{} Care Gr'.format(instance.user.first_name)
        group_room.room_img = instance.patient_profile.prof_pic
        group_room.consumers.add(p_consumer)
        p_contact_gr = Contact.objects.create(contact_name='{} Care Gr'.format(instance.user.first_name), contact_img=instance.patient_profile.prof_pic)
        p_contact_gr.contact_profiles.add(instance.user.contact_profile)
        for doc in instance.care_team.care_doctors.all():
            p_contact = Contact.objects.create()
            p_contact.contact_profiles.add(doc.user.contact_profile)
            p_contact.contact_profiles.add(instance.user.contact_profile)
            p_contact.save()
            p_contact_gr.contact_profiles.add(doc.user.contact_profile)
            room = ChatRoom.objects.create()
            room.consumers.add(p_consumer)
            room.consumers.add(doc.user.chat_consumer)
            group_room.consumers.add(doc.user.chat_consumer)
            room.save()
        p_contact_gr.save()
        group_room.save()

@receiver(post_save, sender=CareDoctor)
def doctor_modified_function(sender, instance, created, **kwargs):
    if created:
        from izzycom.models import ContactProfile
        from izzychat.models import ChatConsumer
        ContactProfile.objects.create(user=instance.user, identifier='izzycom-doc-{}'.format(instance.user.username))
        ChatConsumer.objects.create(user=instance.user)

@receiver(m2m_changed, sender=CareTeam.care_doctors.through)
def care_team_doctors_changed(sender, **kwargs):
    from izzycom.models import Contact, ContactProfile
    from izzychat.models import ChatRoom, ChatConsumer
    instance = kwargs['instance']
    for care_doc in instance.care_doctors.all():
        for care_doctor in instance.care_doctors.all():
            doc_contact_profile_pk_list = []
            doc_consumer_pk_list = []
            if care_doctor != care_doc:
                doc_contact_profile_pk_list.append(care_doctor.user.contact_profile.pk)
                doc_contact_profile_pk_list.append(care_doc.user.contact_profile.pk)
                doc_consumer_pk_list.append(care_doctor.user.chat_consumer.pk)
                doc_consumer_pk_list.append(care_doc.user.chat_consumer.pk)
                contacts_qs = Contact.objects.annotate(count=Count('contact_profiles')).filter(count=len(doc_contact_profile_pk_list))
                for pk in doc_contact_profile_pk_list:
                    contacts_qs = contacts_qs.filter(contact_profiles__pk=pk)
                if len(contacts_qs) < 1:
                    contact = Contact.objects.create()
                    for pk in doc_contact_profile_pk_list:
                        contact.contact_profiles.add(ContactProfile.objects.get(pk=pk))
                    contact.save()
                chat_rooms_qs = ChatRoom.objects.annotate(count=Count('consumers')).filter(count=len(doc_consumer_pk_list))
                for pk in doc_consumer_pk_list:
                    chat_rooms_qs = chat_rooms_qs.filter(consumers__pk=pk)
                if len(chat_rooms_qs) < 1:
                    chat_room = ChatRoom.objects.create()
                    for pk in doc_consumer_pk_list:
                        chat_room.consumers.add(ChatConsumer.objects.get(pk=pk))
                    chat_room.save()
    if kwargs['action'] == 'post_remove':
        for doc in instance.care_doctors.all():
            for doc_pk in kwargs['pk_set']:
                doc_contact_profile_pk_list = []
                doc_consumer_pk_list = []
                doc_contact_profile_pk_list.append(doc.user.contact_profile.pk)
                doc_contact_profile_pk_list.append(CareDoctor.objects.get(pk=doc_pk).user.contact_profile.pk)
                doc_consumer_pk_list.append(doc.user.chat_consumer.pk)
                doc_consumer_pk_list.append(CareDoctor.objects.get(pk=doc_pk).user.chat_consumer.pk)
                contacts_qs = Contact.objects.annotate(count=Count('contact_profiles')).filter(count=len(doc_contact_profile_pk_list))
                for pk in doc_contact_profile_pk_list:
                    contacts_qs = contacts_qs.filter(contact_profiles__pk=pk)
                for doc_contact in contacts_qs:
                    doc_contact.delete()
                chat_rooms_qs = ChatRoom.objects.annotate(count=Count('consumers')).filter(count=len(doc_consumer_pk_list))
                for pk in doc_consumer_pk_list:
                    chat_rooms_qs = chat_rooms_qs.filter(consumers__pk=pk)
                for doc_chat_room in chat_rooms_qs:
                    doc_chat_room.delete()
        for pat in instance.care_patients.all():
            for doc_pk in kwargs['pk_set']:
                doc = CareDoctor.objects.get(pk=doc_pk)
                dp_contacts = Contact.objects.filter(contact_profiles__in=[doc.user.contact_profile, pat.user.contact_profile])
                dp_chat_rooms = ChatRoom.objects.filter(consumers__in=[doc.user.chat_consumer, pat.user.chat_consumer])
                for con in dp_contacts:
                    con.delete()
                for room in dp_chat_rooms:
                    room.delete()
    for care_patient in instance.care_patients.all():
        contact_profile_pk_list = []
        consumer_pk_list = []
        contact_profile_pk_list.append(care_patient.user.contact_profile.pk)
        consumer_pk_list.append(care_patient.user.chat_consumer.pk)
        for care_doctor in instance.care_doctors.all():
            contact_profile_pk_list.append(care_doctor.user.contact_profile.pk)
            consumer_pk_list.append(care_doctor.user.chat_consumer.pk)
        contacts_qs = Contact.objects.annotate(count=Count('contact_profiles')).filter(count=len(contact_profile_pk_list))
        for pk in contact_profile_pk_list:
            contacts_qs = contacts_qs.filter(contact_profiles__pk=pk)
        if len(contacts_qs) < 1 and len(contact_profile_pk_list) == 4:
            contact = Contact.objects.create(contact_name='{} Care Gr'.format(care_patient.user.first_name), contact_img=care_patient.patient_profile.prof_pic)
            for gr_pk in contact_profile_pk_list:
                contact.contact_profiles.add(ContactProfile.objects.get(pk=gr_pk))
            contact.save()
        chat_rooms_qs = ChatRoom.objects.annotate(count=Count('consumers')).filter(count=len(consumer_pk_list))
        for pk in consumer_pk_list:
            chat_rooms_qs = chat_rooms_qs.filter(consumers__pk=pk)
        if len(chat_rooms_qs) < 1 and len(consumer_pk_list) == 4:
            chat_room = ChatRoom.objects.create(room_name='{} Care Gr'.format(care_patient.user.first_name), room_img=care_patient.patient_profile.prof_pic)
            for gr_pk in consumer_pk_list:
                chat_room.consumers.add(ChatConsumer.objects.get(pk=gr_pk))
            chat_room.save()
    for care_doctor in instance.care_doctors.all():
        for care_patient in instance.care_patients.all():
            contact_profile_pk_list = []
            consumer_pk_list = []
            contact_profile_pk_list.append(care_patient.user.contact_profile.pk)
            contact_profile_pk_list.append(care_doctor.user.contact_profile.pk)
            consumer_pk_list.append(care_doctor.user.chat_consumer.pk)
            consumer_pk_list.append(care_patient.user.chat_consumer.pk)
            contacts_qs = Contact.objects.annotate(count=Count('contact_profiles')).filter(count=len(contact_profile_pk_list))
            for pk in contact_profile_pk_list:
                contacts_qs = contacts_qs.filter(contact_profiles__pk=pk)
            if len(contacts_qs) < 1:
                contact = Contact.objects.create()
                for dp_pk in contact_profile_pk_list:
                    contact.contact_profiles.add(ContactProfile.objects.get(pk=dp_pk))
                contact.save()
            chat_rooms_qs = ChatRoom.objects.annotate(count=Count('consumers')).filter(count=len(consumer_pk_list))
            for pk in consumer_pk_list:
                chat_rooms_qs = chat_rooms_qs.filter(consumers__pk=pk)
            if len(chat_rooms_qs) < 1:
                chat_room = ChatRoom.objects.create()
                for dp_pk in consumer_pk_list:
                    chat_room.consumers.add(ChatConsumer.objects.get(pk=dp_pk))
                chat_room.save()