from django.urls import path

from izzychat import consumers

websocket_urlpatterns = [
    path('chat/<str:room_name>/', consumers.IzzyChatConsumer),
]