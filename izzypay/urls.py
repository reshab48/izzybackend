from django.urls import path
from izzypay import views

urlpatterns = [
    path('api/v1/set-payment-card/', views.PaymentCardSetView.as_view(), name='set-payment-card'),
]