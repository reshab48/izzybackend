from django.shortcuts import render, get_object_or_404
from rest_framework import generics, exceptions, status
from rest_framework.views import APIView
from rest_framework.response import Response
from izzychat.models import ChatRoom, ChatConsumer, ChatMessage, ChatNote
from izzychat.serializers import ChatRoomSerializer, ChatMessageSerializer, ChatNoteSerializer
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import IsAuthenticated
from common import permissions

# Create your views here.
class ListUserChatRooms(generics.ListAPIView):
    permission_classes = (IsAuthenticated, permissions.IsCareDoctorOrPatient)
    serializer_class = ChatRoomSerializer
    name = 'list-user-chat-rooms'

    def get_queryset(self):
        return ChatRoom.objects.filter(consumers__in=[get_object_or_404(ChatConsumer, user=self.request.user)]).distinct().order_by('-chat_messages__created')

class ChatMessagePagination(PageNumberPagination):
    page_size = 100

class ListRoomChatMessages(generics.ListAPIView):
    permission_classes = (IsAuthenticated, permissions.IsCareDoctorOrPatient)
    serializer_class = ChatMessageSerializer
    pagination_class = ChatMessagePagination
    lookup_url_kwarg = 'uid'
    name = 'list-room-chat-messages'

    def get_queryset(self):
        return ChatMessage.objects.filter(chat_room=get_object_or_404(ChatRoom, id=self.kwargs.get(self.lookup_url_kwarg))).order_by('created')

class SaveChatMessage(APIView):
    permission_classes = [IsAuthenticated, permissions.IsCareDoctorOrPatient]
    name = 'save-chat-message'

    def post(self, request, format=None):
        try:
            message_ids = request.data.get('message_ids')
        except Exception as e:
            raise exceptions.APIException(detail='message_ids cannot be blank', code=400)
        if not type(message_ids) == list:
            raise exceptions.APIException(detail='message_ids must be a list of values', code=400)
        for msg_id in message_ids:
            ChatNote.objects.create(user=request.user, chat_message_id=msg_id)
        return Response(status=status.HTTP_204_NO_CONTENT)

class ListChatNotes(generics.ListAPIView):
    permission_classes = [IsAuthenticated, permissions.IsCareDoctorOrPatient]
    serializer_class = ChatNoteSerializer
    name = 'list-chat-notes'

    def get_queryset(self):
        return ChatNote.objects.filter(user=self.request.user).select_related('chat_message__consumer__user__care_patient__patient_profile', 'chat_message__consumer__user__care_doctor')