from django.urls import path
from izzyadmin.views import CustomLoginView, dashboard, izzy_doctors_list, create_izzy_doctor

urlpatterns = [
    path('', dashboard, name='dashboard'),
    path('login/', CustomLoginView.as_view(), name='login'),
    path('doctors/', izzy_doctors_list, name='doctors-list'),
    path('doctors/create/', create_izzy_doctor, name='doctor-create'),
]