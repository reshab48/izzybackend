from django.apps import AppConfig


class IzzycomConfig(AppConfig):
    name = 'izzycom'
