from django.urls import path
from accounts import views

urlpatterns = [
    path('api/v1/<int:age>/membership-detail/', views.MembershipDetailView.as_view(), name='membership-detail'),
    path('api/v1/list-izzy-country/', views.ListIzzyCountryView.as_view(), name='list-izzy-country'),
    path('api/v1/list-izzy-state/', views.ListIzzyStateView.as_view(), name='list-izzy-sate'),
    path('api/v1/account-login/', views.CustomLoginView.as_view(), name='login'),
    path('api/v1/profile-detail/', views.ProfileDetailView.as_view(), name='profile-detail-view'),
    path('api/v1/create-care-patient/', views.CarePatientCreateView.as_view(), name='create-care-patient'),
    path('api/v1/create-patient-profile/', views.CarePatientProfileCreateView.as_view(), name='create-patient-profile'),
    path('api/v1/create-patient-prescription/', views.PrescriptionCreateView.as_view(), name='create-patient-prescription'),
    path('api/v1/doctor-patients-list/', views.DoctorPatientsListView.as_view(), name='doctor-patients-list'),
    path('api/v1/prescription-list/', views.PrescriptionListView.as_view(), name='prescription-list'),
    path('api/v1/password-reset/', views.IzzyPasswordResetView.as_view(), name='password-reset'),
    path('api/v1/password-reset-confirm/', views.IzzyPasswordResetConfirmView.as_view(), name='password-reset-confirm'),
]