from django.db import models
from accounts.models import CarePatient
from django.db.models.signals import post_save
from django.dispatch import receiver

# Create your models here.
class Payment(models.Model):
    care_patient = models.ForeignKey(CarePatient, related_name='payments', on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=100, decimal_places=2)
    due = models.BooleanField(default=True)
    charge_id = models.CharField(max_length=128, blank=True)
    refund_id = models.CharField(max_length=128, blank=True)
    refunded = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now=True)
    updated = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'izzy_payment'
        ordering = ('-created', '-updated')

    def __str__(self):
        return str(self.care_patient)

class PaymentMode(models.Model):
    care_patient = models.OneToOneField(CarePatient, related_name='payment_mode', on_delete=models.CASCADE)
    pay_monthly = models.BooleanField(default=True)
    card_number = models.CharField(max_length=16)
    exp_month = models.IntegerField()
    exp_year = models.IntegerField()
    cvc = models.CharField(max_length=3)
    brand = models.CharField(max_length=50)

    class Meta:
        db_table = 'izzy_payment_mode'

    def __str__(self):
        return str(self.care_patient)

@receiver(post_save, sender=PaymentMode)
def payment_mode_modified_function(sender, instance, created, **kwargs):
    if created:
        if instance.care_patient.patient_profile.age < 20:
            if instance.pay_monthly:
                if instance.care_patient.membership.charge_actual:
                    Payment.objects.create(care_patient=instance.care_patient, amount=instance.care_patient.membership.actual_teen_price - 1)
                else:
                    Payment.objects.create(care_patient=instance.care_patient, amount=instance.care_patient.membership.intro_teen_price - 1)
            else:
                if instance.care_patient.membership.charge_actual:
                    Payment.objects.create(care_patient=instance.care_patient, amount=instance.care_patient.membership.actual_teen_yearly_price - 1)
                else:
                    Payment.objects.create(care_patient=instance.care_patient, amount=instance.care_patient.membership.intro_teen_yearly_price - 1)
        else:
            if instance.pay_monthly:
                if instance.care_patient.membership.charge_actual:
                    Payment.objects.create(care_patient=instance.care_patient, amount=instance.care_patient.membership.actual_adult_price - 1)
                else:
                    Payment.objects.create(care_patient=instance.care_patient, amount=instance.care_patient.membership.intro_adult_price - 1)
            else:
                if instance.care_patient.membership.charge_actual:
                    Payment.objects.create(care_patient=instance.care_patient, amount=instance.care_patient.membership.actual_adult_yearly_price - 1)
                else:
                    Payment.objects.create(care_patient=instance.care_patient, amount=instance.care_patient.membership.intro_adult_yearly_price - 1)