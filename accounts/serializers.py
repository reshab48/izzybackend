from rest_auth.models import TokenModel
from rest_framework import serializers, exceptions
from django.contrib.auth.models import User
from accounts.models import CarePatient, IzzyPatientProfile, Membership, IzzyState, IzzyCountry, CareTeam, PatientPrescription
from django.contrib.sites.shortcuts import get_current_site
from izzypay.models import Payment
from django.db.models import Count
from django.conf import settings
import random
import string


GENS = (
    ('ML', 'Male'),
    ('FM', 'Female'),
)

class TokenSerializer(serializers.ModelSerializer):
    user_type = serializers.SerializerMethodField()
    name = serializers.SerializerMethodField()
    profile_pic = serializers.SerializerMethodField()
    age = serializers.SerializerMethodField()
    sex = serializers.SerializerMethodField()
    doc_bio = serializers.SerializerMethodField()
    patient_height = serializers.SerializerMethodField()
    patient_weight = serializers.SerializerMethodField()
    patient_allergies = serializers.SerializerMethodField()
    patient_medication_allergies = serializers.SerializerMethodField()
    patient_diseases = serializers.SerializerMethodField()

    def get_age(self, instance):
        request = self.context.get('request')
        if hasattr(request, 'user'):
            if hasattr(request.user, 'care_patient'):
                return request.user.care_patient.patient_profile.age
            elif hasattr(request.user, 'care_doctor'):
                return request.user.care_doctor.age
            else:
                raise exceptions.PermissionDenied("Izzy User not found")
        else:
            raise exceptions.PermissionDenied("User not found")

    def get_sex(self, instance):
        request = self.context.get('request')
        if hasattr(request, 'user'):
            if hasattr(request.user, 'care_patient'):
                return request.user.care_patient.patient_profile.sex
            elif hasattr(request.user, 'care_doctor'):
                return request.user.care_doctor.gender
            else:
                raise exceptions.PermissionDenied("Izzy User not found")
        else:
            raise exceptions.PermissionDenied("User not found")

    def get_doc_bio(self, instance):
        request = self.context.get('request')
        if hasattr(request, 'user'):
            if hasattr(request.user, 'care_patient'):
                return ''
            elif hasattr(request.user, 'care_doctor'):
                return request.user.care_doctor.bio
            else:
                raise exceptions.PermissionDenied("Izzy User not found")
        else:
            raise exceptions.PermissionDenied("User not found")

    def get_patient_height(self, instance):
        request = self.context.get('request')
        if hasattr(request, 'user'):
            if hasattr(request.user, 'care_patient'):
                return request.user.care_patient.patient_profile.height
            elif hasattr(request.user, 'care_doctor'):
                return ''
            else:
                raise exceptions.PermissionDenied("Izzy User not found")
        else:
            raise exceptions.PermissionDenied("User not found")

    def get_patient_weight(self, instance):
        request = self.context.get('request')
        if hasattr(request, 'user'):
            if hasattr(request.user, 'care_patient'):
                return request.user.care_patient.patient_profile.weight
            elif hasattr(request.user, 'care_doctor'):
                return ''
            else:
                raise exceptions.PermissionDenied("Izzy User not found")
        else:
            raise exceptions.PermissionDenied("User not found")  

    def get_patient_allergies(self, instance):
        request = self.context.get('request')
        if hasattr(request, 'user'):
            if hasattr(request.user, 'care_patient'):
                return request.user.care_patient.patient_profile.allergies
            elif hasattr(request.user, 'care_doctor'):
                return ''
            else:
                raise exceptions.PermissionDenied("Izzy User not found")
        else:
            raise exceptions.PermissionDenied("User not found")  

    def get_patient_medication_allergies(self, instance):
        request = self.context.get('request')
        if hasattr(request, 'user'):
            if hasattr(request.user, 'care_patient'):
                return request.user.care_patient.patient_profile.medication_allergies
            elif hasattr(request.user, 'care_doctor'):
                return ''
            else:
                raise exceptions.PermissionDenied("Izzy User not found")
        else:
            raise exceptions.PermissionDenied("User not found")

    def get_patient_diseases(self, instance):
        request = self.context.get('request')
        if hasattr(request, 'user'):
            if hasattr(request.user, 'care_patient'):
                return request.user.care_patient.patient_profile.heredity_diseases
            elif hasattr(request.user, 'care_doctor'):
                return ''
            else:
                raise exceptions.PermissionDenied("Izzy User not found")
        else:
            raise exceptions.PermissionDenied("User not found")

    def get_user_type(self, instance):
        request = self.context.get('request')
        if hasattr(request, 'user'):
            if hasattr(request.user, 'care_patient'):
                return 'care_patient'
            elif hasattr(request.user, 'care_doctor'):
                return 'care_doctor'
            else:
                raise exceptions.PermissionDenied("Izzy User not found")
        else:
            raise exceptions.PermissionDenied("User not found")

    def get_name(self, instance):
        request = self.context.get('request')
        if hasattr(request, 'user'):
            if hasattr(request.user, 'care_patient'):
                return '{} {}'.format(request.user.first_name, request.user.last_name)
            elif hasattr(request.user, 'care_doctor'):
                return 'Dr. {} {}'.format(request.user.first_name, request.user.last_name)
            else:
                raise exceptions.PermissionDenied("Izzy User not found")
        else:
            raise exceptions.PermissionDenied("User not found")

    def get_profile_pic(self, instance):
        request = self.context.get('request')
        if hasattr(request, 'user'):
            if hasattr(request.user, 'care_patient'):
                try:
                    return request.user.care_patient.patient_profile.prof_pic.url
                except:
                    return ""
            elif hasattr(request.user, 'care_doctor'):
                return request.user.care_doctor.prof_pic.url
            else:
                raise exceptions.PermissionDenied("Izzy User not found")
        else:
            raise exceptions.PermissionDenied("User not found")

    class Meta:
        model = TokenModel
        fields = ('key', 'user_type', 'name', 'profile_pic', 'age', 'sex', 'doc_bio', 'patient_height', 'patient_weight', 'patient_allergies', 'patient_medication_allergies', 'patient_diseases')

class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)

    class Meta:
        model = User
        fields = ('email', 'first_name', 'last_name', 'password')

class CarePatientProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = IzzyPatientProfile
        fields = '__all__'
        read_only_fields = ('created', 'updated')

class CarePatientSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = CarePatient
        exclude = ('id', 'membership', 'care_team', 'country')
        read_only_fields = ('created', 'updated')

    def create(self, validated_data):
        user_validated_data = validated_data.pop('user')
        if User.objects.filter(email=user_validated_data['email']).exists():
            raise exceptions.ParseError("Email already exists, use a different email.")
        user = User.objects.create(username=user_validated_data['email'], email=user_validated_data['email'], first_name=user_validated_data['first_name'], last_name=user_validated_data['last_name'], is_active=False)
        user.set_password(user_validated_data['password'])
        user.save()
        validated_data['user'] = user
        country = get_current_site(self.context.get('request')).membership.izzy_country
        validated_data['country'] = country
        validated_data['membership'] = Membership.objects.get(izzy_country=country)
        care_teams = CareTeam.objects.annotate(care_patients_count=Count('care_patients')).filter(care_patients_count__lte=1500, country=country)
        if country.country.code == 'US':
            state = validated_data['state']
            care_teams = CareTeam.objects.filter(state=state)
        ran_int = random.randint(0, len(care_teams)-1)
        validated_data['care_team'] = care_teams[ran_int]
        patient = CarePatient.objects.create(**validated_data)
        return patient

class IzzyStateSerializer(serializers.ModelSerializer):
    class Meta:
        model = IzzyState
        fields = '__all__'

class IzzyCountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = IzzyCountry
        fields = '__all__'

class MedicationDataSerializer(serializers.Serializer):
    name = serializers.CharField()
    dosage = serializers.CharField()

class PrescriptionSerializer(serializers.Serializer):
    pat_email = serializers.EmailField()
    meds = MedicationDataSerializer(many=True)

class PrescriptionListSerializer(serializers.ModelSerializer):
    full_name = serializers.SerializerMethodField()
    pic = serializers.SerializerMethodField()
    pdf_file = serializers.SerializerMethodField()

    def get_full_name(self, instance):
        request = self.context.get('request')
        if hasattr(request.user, 'care_doctor'):
            return '{} {}'.format(instance.patient.user.first_name, instance.patient.user.last_name)
        else:
            return 'Dr. {} {}'.format(instance.doctor.user.first_name, instance.doctor.user.last_name)
    
    def get_pic(self, instance):
        request = self.context.get('request')
        if hasattr(request.user, 'care_doctor'):
            return request.build_absolute_uri(instance.patient.patient_profile.prof_pic.url)
        else:
            return request.build_absolute_uri(instance.doctor.prof_pic.url)

    def get_pdf_file(self, instance):
        request = self.context.get('request')
        return request.build_absolute_uri('{}{}'.format(settings.MEDIA_URL, instance.pdf_path))

    class Meta:
        model = PatientPrescription
        fields = ('full_name', 'pic', 'created', 'pdf_file')