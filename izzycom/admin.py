from django.contrib import admin
from izzycom.models import ContactProfile, Contact, PatientAppointment

# Register your models here.
admin.site.register(ContactProfile)
admin.site.register(Contact)
admin.site.register(PatientAppointment)