from django.apps import AppConfig


class IzzychatConfig(AppConfig):
    name = 'izzychat'
